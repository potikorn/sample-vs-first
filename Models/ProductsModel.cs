namespace sample.Models {
    public class ProductsModel {
        public int ID { get; set; }
        public string Name {get; set;}
        public string Logo {get; set;}
        public string Image {get; set;}
        public string Detail {get; set;}
        public string Code {get; set;}
        public string Type {get; set;}
    }
}