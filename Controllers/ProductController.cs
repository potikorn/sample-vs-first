using Microsoft.AspNetCore.Mvc;
using sample.Models;

namespace sample.Controllers
{
    public class ProductController: Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Detail()
        {
            ProductsModel data = new ProductsModel();
            data.ID = 1;
            data.Name = "Product 1";
            data.Detail = "Pet Feed";
            data.Code = "P-001";
            data.Type = "Dog feed";
            data.Image = "http://www.feedmeplease.com/images/pdimg/764/1.jpg";
            data.Logo = "http://www.alwayspetshop.com/image/cache/data/APRO-873x312.jpg";
            return View(data);
        }
    }
}